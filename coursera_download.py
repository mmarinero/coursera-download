#!/usr/bin/env python3

# Installation
# Execute pip3 install selenium or similar, the 3 might be necessary
# Download geckodriver from https://github.com/mozilla/geckodriver/releases
# and place it the $PATH

from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
from time import sleep
from typing import List, Optional
from pathlib import Path
from urllib.request import urlretrieve
from urllib.parse import urlparse, parse_qs
import string
import json
import logging
import sys
import pathlib
import getpass
import argparse


def get_browser():
    """Support repl use"""
    return browser


def login(browser, url, email):
    """Logs into coursera.org with email and password"""
    password = getpass.getpass('Password:')
    browser.get(url)

    email_field = browser.find_element_by_name("email")
    email_field.clear()
    email_field.send_keys(email)

    password_field = browser.find_element_by_name("password")
    password_field.clear()
    password_field.send_keys(password)
    browser.find_element_by_name("login").submit()


class Course:
    """Scraps courses from Coursera looking for the course resources links.
    Generates a data structure with all the links that can be stored.
    Downloads all the resources found into a directories for each course week.
    It probably needs to be tuned for specific courses and when Coursera
    updates it's web. Inheritance can be used to solve small problems for a
    course but it's probably better to use this class as a template if deep
    changes are needed
    """

    def __init__(self, browser, logger, course={}):
        """browser is a selenium WebDriver instance and course an optional
        scrapped course to resume previous downloads"""
        self.browser = browser
        self.logger = logger
        self.course = course

    def _select(self, selector):
        return self.browser.find_elements_by_css_selector(selector)

    def _selectOne(self, selector):
        return self.browser.find_element_by_css_selector(selector)

    def _hrefs(self, aElements):
        """Get the link from a list of a html elements"""
        return [a.get_attribute('href') for
                a in aElements]

    def _weeks(self) -> List[str]:
        """Fetch weeks urls, can be done scrapping the weeks menu
        but it is predictable in many cases so a simple loop might
        suffice"""
        return self._hrefs(self._select('a.rc-WeekNavigationItem'))

    def _safe_name(self, name: str) -> str:
        """ Shameless copy from stackoverflow to avoid problems with names.
        Not completely safe but I don't think there are many uncommon names and
        characters in the lecture titles
        https://stackoverflow.com/questions/295135/turn-a-string-into-a-valid-filename
        """
        valid_chars = "-_.() %s%s" % (string.ascii_letters, string.digits)
        return ''.join(c for c in name if c in valid_chars)

    def _get_extension(self, url: str) -> Optional[str]:
        """Attempt to get the extension from the url Extension param"""
        parsed_url = urlparse(url)
        qs = parse_qs(parsed_url.query)
        if 'fileExtension' in qs:
            return qs['fileExtension'][0]
        else:
            return None

    def _videos(self):
        """Fetch video page urls from a weeks page, it may pick assignments
        links that will be filtered out on the video_links step"""
        links = self._hrefs(self._select('a.rc-ItemLink'))
        names = [f"{i + 1}. {self._safe_name(el.text)}"
                 for i, el in enumerate(self._select('a.rc-ItemLink h5 span'))]
        return [{'link': link, 'name': name}
                for (link, name) in zip(links, names)]

    def _video_links(self, lecture_name):
        """Fetch video, subtitle, slides download links once on a video page.
        The normal download button provides a low quality version for some
        courses so we will click the high quality button and get the video
        source directly"""
        wait = WebDriverWait(self.browser, 20)
        try:
            wait.until(EC.presence_of_element_located(
                (By.CSS_SELECTOR, ".c-resolution-button")))
        except TimeoutException:
            # Assume no video in the page, but could be that coursera
            # changed the video player
            self.logger.warning("No video found in the page," +
                                "can be a execises page or a change" +
                                "in coursera video player")
            return []

        # Using actions all I got was "TypeError: rect is undefined"
        # Feel happy to use any othe way to find the high quality url
        script = ("document." +
                  "getElementsByClassName('c-resolution-button')[1].click();")
        # The video binds the click well after rendering the button
        sleep(5)
        self.browser.execute_script(script)
        video_url = self._selectOne('video').get_attribute('currentSrc')
        links = {f"{lecture_name}.mp4": video_url}
        menu_links = self._hrefs(
            self._select('.resources-list a.resource-link'))
        # Remove the video menu item as it is downloaded from the video tag
        menu_links.pop(0)
        for i, link in enumerate(menu_links):
            # pdf is a good default as slides seems to not have extension
            # but other courses may differ
            extension = self._get_extension(link) or "pdf"
            name = f"{lecture_name}.{extension}"
            if (name in links):
                name = f"{lecture_name}.{i}.{extension}"
            links[name] = link
        return links

    def scrap(self):
        """Iterate over course weeks into weeks videos pages to
        create the self.course structure with every link that will
        be downloaded afterwards"""
        weeks = self._weeks()
        course = self.course
        for i, week in enumerate(weeks):
            self.logger.info(f"Entering week: {i + 1}, url: {week}")
            self.browser.get(week)
            videos = self._videos()
            week_videos = {}
            for video in videos:
                self.logger.info(f"Entering video page: {video['name']}")
                self.browser.get(video['link'])
                links = self._video_links(video['name'])
                if links:
                    week_videos[video['name']] = links
            if week_videos:
                course[i] = week_videos
        self.logger.info(f"Course has been scrapped")

    def download_all(self, parent_dir):
        for i, week in enumerate(self.course):
            week_path = parent_dir / f"week{i + 1}"
            logger.info(f"Downloading week {i + 1}")
            week_path.mkdir(exist_ok=True)
            for name, urls in self.course[week].items():
                lecture_dir = week_path / name
                lecture_dir.mkdir(exist_ok=True)
                for name, url in urls.items():
                    urlretrieve(url, lecture_dir / name)


if __name__ == "__main__":

    def get_parser():
        parser = argparse.ArgumentParser(description='Sync library')
        parser.add_argument('course', help='course url section name in' +
                            '"https://www.coursera.org/learn/scala-spark-' +
                            'big-data/" would be "scala-spark-big-data"')
        parser.add_argument('user', help='coursera username (email) with ' +
                            'access to the course')
        return parser

    args = get_parser().parse_args()
    logger = logging.getLogger('coursera_download')
    logging.basicConfig(stream=sys.stdout, level=logging.INFO)
    browser = webdriver.Firefox()
    browser.implicitly_wait(20)
    wait = WebDriverWait(browser, 20)
    course_name = args.course
    course_base = "https://www.coursera.org/learn"
    course_home = f"{course_base}/{course_name}/home"
    logger.info('Logging in to coursera...')
    root = Path.cwd() / args.course
    root.mkdir(exist_ok=True)

    # Fill in you username
    login(browser, course_home, args.user)
    wait.until(EC.url_changes(course_home))
    course = Course(browser, logger)
    json_path = root / 'course.json'
    if (json_path.is_file()):
        with open(json_path) as infile:
            course.course = json.load(infile)
    else:
        course.scrap()
        with open(json_path, 'w') as outfile:
            json.dump(course.course, outfile, indent=4)

    browser.quit()
    course.download_all(root)
