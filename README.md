Installation
=====

Execute pip3 install selenium or similar, the 3 might be necessary
Download geckodriver from https://github.com/mozilla/geckodriver/releases
and place it in the $PATH

It works with python 3.6+

Usage
===

The script scraps a Coursera course by weeks and lectures and stores
all videos and slides found in course.json, then downloads all the files.

If the course.json file exists it avoids the scrapping. It has been tested with two courses

It's only intended to download the lectures, assignments or other material will probably we ignored but it has only be
tested against two courses with a similar structure.

Small changes to the script can be done by inheriting the Course class but for courses with a different structure or big changes
on the Coursera pages it would be better to write a new version.